#!/bin/bash

# PREREQUISITES
# install s3cmd
# and postgres-client which version corresponds to db version

DB_HOST=$1
DB_PORT=$2
DB_NAME=$3
DB_USER=$4
DB_PASSWORD=$5
TIMESTAMP=$6  # %d-%m-%YT%H-%M-%S

BUCKET_NAME='share-service-backups'

TEMP_FILE=$(mktemp tmp.XXXXXXXXXX)
S3_FILE="s3://$BUCKET_NAME/$TIMESTAMP.dump"

s3cmd get $S3_FILE $TEMP_FILE --continue
echo "dump has been gotten from s3 to file ${TEMP_FILE}"

PGPASSWORD=$DB_PASSWORD psql -d $DB_NAME -h $DB_HOST -p $DB_PORT -U $DB_USER -f $TEMP_FILE
echo "database has been restored"

rm "$TEMP_FILE"
echo "dump file ${TEMP_FILE} has been removed from os"