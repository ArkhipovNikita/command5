from celery import shared_task
from celery.task import task
from celery.utils.log import get_task_logger
from django.db import transaction

from goal.additionally import generate_random_string
from goal.models import GoalParty, GoalPartyRequest, Vote

logger = get_task_logger(__name__)


@shared_task
def goal_party_request():
    requests = GoalPartyRequest.objects.select_related("user", "goal").all()
    parties_count = 0

    for req in requests:
        goal = req.goal
        user = req.user
        parties = GoalParty.objects.select_related("basemodel_ptr", "goal").filter(goal=goal)
        max_members = goal.max_number_of_members
        found = False

        for party in parties:
            ban_list = party.ban.all()
            if party.members.all().count() != max_members and party.public and user not in ban_list:
                party.members.add(user)
                party.save()
                found = True
            if found:
                break

        if not found:
            party = GoalParty(goal=goal, admin=user)
            party.save()
            parties_count += 1
        req.delete()

    logger.info(f"INFO: {parties_count} GoalParties was created")


@shared_task
def goal_party_link_update():
    queryset = GoalParty.objects.select_related().all()
    for goal_party in queryset:
        goal_party.link = generate_random_string()
        goal_party.save()
    logger.info(f"INFO: {len(queryset)} GoalParty links was updated")


@task
def complete_vote(vote_id):
    try:
        vote = Vote.objects.prefetch_related("party", "party__members", "against",).get(id=vote_id)
    except Vote.DoesNotExist:
        return

    with transaction.atomic():
        if vote.should_kick:
            vote.result = True
            vote.party.members.remove(vote.against)
            # return money to user if subscription hasn't started

        vote.result = False
        vote.save()
