import datetime

from django.conf import settings
from django.db import transaction
from django.utils import timezone
from drf_yasg.openapi import IN_QUERY, Parameter
from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, status
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin, DestroyModelMixin, ListModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from goal.models import GoalParty, Voice, Vote
from goal.serializers.vote import (
    VoiceSerializer,
    VoteCreateSerializer,
    VoteRetrieveSerializer,
)
from goal.tasks import complete_vote


# TODO: try to reduce code with super actions
class VoteViewSet(
    GenericViewSet, CreateModelMixin, ListModelMixin, DestroyModelMixin,
):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = VoteRetrieveSerializer
    queryset = Vote.objects.all()

    @swagger_auto_schema(
        request_body=VoteCreateSerializer(), responses={200: VoteRetrieveSerializer()},
    )
    def create(self, request, *args, **kwargs):
        serializer = VoteCreateSerializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)

        with transaction.atomic():
            vote = serializer.create(serializer.validated_data)
            Voice.objects.create(
                vote=vote, voter=request.user, for_or_against=True,
            )

            # TODO: put task id to redis to delete it when destroying
            complete_vote.apply_async((vote.id,), eta=vote.expiration_date)

        vote.refresh_from_db(fields=("for_voice_count",))

        return Response(self.get_serializer(instance=vote).data)

    @swagger_auto_schema(
        manual_parameters=[Parameter("party", IN_QUERY, type="integer")],
        responses={200: VoteRetrieveSerializer(many=True)},
    )
    def list(self, request, *args, **kwargs):
        # TODO: get party id via url parameter
        party_id = request.query_params.get("party")

        try:
            party = GoalParty.objects.prefetch_related("members").get(id=party_id)
        except GoalParty.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        if request.user not in party.members.all():
            return Response(status=status.HTTP_403_FORBIDDEN)

        votes = Vote.objects.filter(
            party__id=party_id,
            expiration_date__gt=timezone.now() - datetime.timedelta(milliseconds=settings.VOTE_SEE_RESULT_DURATION),
        )

        return Response(VoteRetrieveSerializer(votes, many=True).data)

    @swagger_auto_schema(
        request_body=VoiceSerializer(), responses={200: VoiceSerializer()},
    )
    @action(methods=["POST"], detail=False, url_path="voice", url_name="vote")
    def vote(self, request, *args, **kwargs):
        serializer = VoiceSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        serializer.create(serializer.validated_data)

        return Response(serializer.data)
