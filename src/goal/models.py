import datetime
from decimal import Decimal

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django_counter_cache_field import CounterCacheField
from stdimage import StdImageField

from goal.additionally import generate_random_string

User = get_user_model()


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="create_date")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="last_edit_date")

    class Meta:
        abstract = True


class GoalCategory(models.Model):
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to="goal_categories")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "goal categories"


class Goal(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    image = StdImageField(upload_to="goal_images", variations={"thumbnail": {"width": 100, "height": 75}})
    max_number_of_members = models.PositiveSmallIntegerField()
    category = models.ForeignKey(GoalCategory, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.title


class GoalParty(models.Model):
    goal = models.ForeignKey("Goal", on_delete=models.CASCADE)
    admin = models.ForeignKey("main.User", on_delete=models.CASCADE, related_name="admin_of_goal_party")
    members = models.ManyToManyField("main.User", blank=True)
    ban = models.ManyToManyField("main.User", blank=True, related_name="ban_list")
    balance = models.DecimalField(max_digits=7, decimal_places=2, default=Decimal("0.00"))
    link = models.CharField(max_length=255, blank=True, default=generate_random_string)
    public = models.BooleanField(default=True)

    # TODO: add members count field

    @property
    def size(self):
        return self.members.count()

    @property
    def has_active_subscription(self):
        last_subscription = self.subscriptions.order_by("-end_date").first()
        return last_subscription is not None and last_subscription.end_date > timezone.now()

    def __str__(self):
        return f"Party for goal ({self.goal}) with admin ({self.admin})"

    class Meta:
        verbose_name_plural = _("goal parties")


class GoalPartyRequest(models.Model):
    goal = models.ForeignKey("Goal", on_delete=models.CASCADE)
    user = models.ForeignKey("main.User", on_delete=models.CASCADE)

    def __str__(self):
        return f"{_('Request to party')} {self.goal} {_('from')} {self.user}"


# TODO: replace with payment.models.UserPaymentHistory
class PaymentByCard(models.Model):
    goal_party = models.ForeignKey("GoalParty", on_delete=models.CASCADE)
    user = models.ForeignKey("main.User", on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=7, decimal_places=2)

    def __str__(self):
        return f"{_('Pay for goal party')} {self.goal_party}"


# Vote to kick
class Vote(models.Model):
    against = models.ForeignKey(User, on_delete=models.CASCADE, related_name="votes_against_me")
    initiator = models.ForeignKey(User, on_delete=models.CASCADE, related_name="votes_initiated_by_me")
    party = models.ForeignKey(GoalParty, on_delete=models.CASCADE)
    expiration_date = models.DateTimeField()
    comment = models.CharField(max_length=255)
    for_voice_count = CounterCacheField()
    against_voice_count = CounterCacheField()
    result = models.BooleanField(null=True)

    @property
    def should_kick(self):
        return self.for_voice_count >= self.party.size * settings.VOTE_MIN_PERCENTAGE_TO_KICK

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.expiration_date = timezone.now() + datetime.timedelta(seconds=settings.VOTE_DURATION)

        return super(Vote, self).save(force_insert, force_update, using, update_fields)


class Voice(models.Model):
    vote = models.ForeignKey(Vote, on_delete=models.CASCADE)
    voter = models.ForeignKey(User, on_delete=models.CASCADE)
    for_or_against = models.BooleanField()

    class Meta:
        unique_together = (
            "vote",
            "voter",
        )
