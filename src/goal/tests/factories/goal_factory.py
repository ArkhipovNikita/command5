import factory
from factory.django import DjangoModelFactory
from faker import Factory

from goal.models import Goal
from goal.tests.factories.category_factory import GoalCategoryFactory

faker = Factory.create()


class GoalFactory(DjangoModelFactory):
    title = factory.Faker("name")
    category = factory.SubFactory(GoalCategoryFactory)
    image = factory.django.ImageField(color="blue")
    max_number_of_members = faker.random_number()

    class Meta:
        model = Goal
