from rest_framework import serializers
from rest_framework.authtoken.models import Token as DRFToken

from main.enums import TokenEnum
from main.models import User
from main.serializers.token import TokenSerializer
from main.serializers.user import UserSerializer
from main.services.validators import validate_email_existing, validate_password


class UserSignUpSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "first_name",
            "last_name",
            "email",
            "password",
        )

    def save(self, **kwargs):
        data = self.validated_data
        data["username"] = data.get("email")
        self.instance = User.objects.create_user(**data)

        return self.instance

    def validate(self, attrs):
        attrs = super().validate(attrs)
        validate_password(attrs.get("password"), attrs)

        return attrs


class UserSignInSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(min_length=1, write_only=True)


class UserAuthOutputSerializer(UserSerializer):
    token = serializers.SerializerMethodField()

    class Meta(UserSerializer.Meta):
        fields = UserSerializer.Meta.fields + ("token",)

    def get_token(self, obj):
        token, created = DRFToken.objects.get_or_create(user=self.instance)
        return token.key


class ConfirmEmailAPISerializer(serializers.Serializer):
    token = serializers.CharField(min_length=1)


class ConfirmEmailSerializer(serializers.Serializer):
    token = TokenSerializer(TokenEnum.EMAIL_CONFIRMATION)

    def to_internal_value(self, data):
        data = dict(token=dict(value=data["token"]))
        return super().to_internal_value(data)


class ForgotPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField(validators=[validate_email_existing])


class SetNewPasswordAPISerializer(serializers.Serializer):
    token = serializers.CharField(min_length=1)
    password = serializers.CharField(min_length=1)


class SetNewPasswordSerializer(serializers.Serializer):
    token = TokenSerializer(TokenEnum.FORGOT_PASSWORD)
    password = serializers.CharField(min_length=1)

    def to_internal_value(self, data):
        data = dict(password=data["password"], token=dict(value=data["token"]))
        return super().to_internal_value(data)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        validate_password(attrs["password"], dict(email=self.fields["token"].instance.user.email))

        return attrs
