from rest_framework import serializers

from main.models import User


class StdImageSerializer(serializers.ImageField, serializers.ReadOnlyField):
    origin = serializers.URLField(source="url")
    thumbnail = serializers.ImageField(source="thumbnail")

    def to_representation(self, value):
        origin_url = super().to_representation(value)
        thumbnail_url = None
        if origin_url:
            thumbnail_url = self.thumbnail.to_representation(value.thumbnail)
        return dict(origin=origin_url, thumbnail=thumbnail_url)


class UserSerializer(serializers.ModelSerializer):
    avatar = StdImageSerializer()

    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "is_email_verified",
            "avatar",
            "rating_score",
        )
        extra_kwargs = {"is_email_verified": {"read_only": True}, "email": {"read_only": True}}


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "avatar")
