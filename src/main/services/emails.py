from django.template.loader import render_to_string
from django.utils.translation import gettext_lazy as _

from main.enums import TokenEnum
from main.models import Token
from main.tasks import send_email


def send_confirm_email(request, user):
    def _generate_confirmation_link():
        token, created = Token.objects.get_or_create(user=user, token_type=TokenEnum.EMAIL_CONFIRMATION)
        return f"{request.META.get('HTTP_ORIGIN')}/auth/confirmation/{token.value}"

    h1 = _("Confirm your email")
    button_text = _("Click here")
    subject = _("Confirm your email")
    confirm_link = _generate_confirmation_link()
    html_message = render_to_string(
        "emails/confirm.html", context=dict(h1=h1, button_text=button_text, button_url=confirm_link,)
    )

    send_email.apply_async((subject, html_message, user.email,))


def send_forgot_email(request, user):
    def _generate_forgot_password_link():
        token, created = Token.objects.get_or_create(user=user, token_type=TokenEnum.FORGOT_PASSWORD)
        return f"{request.META.get('HTTP_ORIGIN')}/auth/forgot_password/{token.value}"

    h1 = _("Reset your password")
    button_text = _("Click here")
    subject = _("Reset your password")
    forgot_password_link = _generate_forgot_password_link()
    html_message = render_to_string(
        "emails/forgot_password.html", context=dict(h1=h1, button_text=button_text, button_url=forgot_password_link,)
    )

    send_email.apply_async((subject, html_message, user.email,))
