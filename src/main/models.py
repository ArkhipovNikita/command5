import datetime
import uuid

from django.conf import settings
from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.utils import timezone
from stdimage import StdImageField

from main.enums import TokenEnum


class CustomUserManager(UserManager):
    def create_superuser(self, email, password=None, **extra_fields):
        super().create_superuser(username=email, email=email, password=password)


class User(AbstractUser):
    email = models.EmailField("email_address", unique=True)
    is_email_verified = models.BooleanField(default=False)
    avatar = StdImageField(
        upload_to="avatars", null=True, blank=True, variations={"thumbnail": {"width": 100, "height": 75}}
    )
    rating_count = models.PositiveIntegerField(default=0)
    rating_score = models.DecimalField(default=0, max_digits=4, decimal_places=2)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    def __str__(self):
        return f"{self.email}"


class Token(models.Model):
    value = models.CharField(max_length=255)
    token_type = models.CharField(max_length=50, choices=TokenEnum.choices)
    expiration_date = models.DateTimeField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="tokens")

    def save(self, *args, **kwargs):
        self.value = uuid.uuid4()
        self.expiration_date = timezone.now() + datetime.timedelta(settings.VERIFICATION_TOKEN_EXPIRATION_TIME)
        super(Token, self).save(*args, **kwargs)


class Rating(models.Model):
    who = models.ForeignKey(User, on_delete=models.CASCADE, related_name="rates")
    whom = models.ForeignKey(User, on_delete=models.CASCADE, related_name="ratings")
    rating = models.PositiveSmallIntegerField()

    class Meta:
        unique_together = (
            "who",
            "whom",
        )
